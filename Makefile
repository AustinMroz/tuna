CLASSES := $(patsubst %.java,%.class,$(wildcard *.java))

%.class : %.java
	javac.exe $^
.PHONY : run
run : $(CLASSES)
	java.exe Tuna
test : $(CLASSES)
	java.exe Test
