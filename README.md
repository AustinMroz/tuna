# Tuna
A work in progress reverse pitch correcter.

## Background
Composing music is currently fairly restrictive without expensive hardware.
While placing distinct notes is possible, it is difficult to replicate the
nuance of changes in or volume between notes.
Things like portamento and vibrato are very hard to implement, and it's
similarly difficult to have any control over volume beyond the initial volume.
Each new note in a crescendo can be louder then the previous,
but putting stuff in between is a difficult feat.
In software I frequently use, it requires the use of controllers that are
separated to the point that you can't even see the notes you are modifying
while you implement these effects.

## Functionality
Tuna aims to fix this by transforming microphone input into MIDI messages
which can then be recorded by external audio software.

While primarily aimed at targeting whistles, which produce pure frequencies
with no overtones, the software calculates pitch based on threshold ed zero
crossings and should work for other sources of sound too.

## Output
The program is still in early development. Currently the midi messages are
sent to the default java midi synthesizer which plays it back in real time.
Using this output as input to other software will likely require manual
setup with external software like JACK.

