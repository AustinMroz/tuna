import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Line;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

import java.util.Arrays;
import java.lang.Math;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.function.Consumer;


public class Tuna {
   public static final int SAMPLE_RATE = 44100;
   public static final int WINDOWS = 30;
   public static final int SAMPLES = SAMPLE_RATE/WINDOWS;
   public static final int LOOPBACK_INDEX = 4;
   public static void main(String[] args) {
      Logger.getGlobal().setLevel(Level.WARNING);
      AudioFormat f = new AudioFormat(44100,16,1,true,false);
      SourceDataLine s = null;
      try {
         //Mixer.Info[] mi = AudioSystem.getMixerInfo();
         //System.out.println(Arrays.toString(mi));
         // Mixer m = AudioSystem.getMixer(mi[mi.length-1]);
         // System.out.println(Arrays.toString(m.getTargetLines()));
         TargetDataLine t = AudioSystem.getTargetDataLine(f);
         t.open(f);
         t.start();

         Tuna tuna = new Tuna();
         tuna.process(pcm -> {t.read(pcm,0,pcm.length); return true;});
         s = AudioSystem.getSourceDataLine(f);
         s.open(f);
         s.start();
      } catch (LineUnavailableException e) {
         System.out.println("Couldn't open device");
         System.exit(-1);
      }
   }
   public void process(SampleProvider t) {
      byte[] pcm = new byte[SAMPLES*2];
      int highest = 0;
      int lowest = 0;
      int hThresh = 0;
      int lThresh = 0;
      int ts = 0;
      int zci = 0;
      int pcross = 0;
      boolean wasPositive = true;
      while(t.read(pcm)) {
         int count = 0;
         int sum = 0;//each sample is 16 bit, Safe so long as frame > ~10/sec
         for (int i = 0;i<SAMPLES;i++) {
            int b = (pcm[i*2+1]<<8)+pcm[i*2];
            if(b<0) {
               if(wasPositive) {
                  if(highest > hThresh) {
                     if(count == 0)
                        pcross+=i>>1;
                     count++;
                     zci = i;
                  }
               }
               wasPositive = false;
               if(b < lowest) {
                  lowest = b;
                  lThresh = (int)(lowest*.75);
               }
            } else {
               if(!wasPositive) {
                  if(lowest < lThresh) {
                     if(count == 0)
                        pcross+=i>>1;
                     count++;
                     zci = i;
                  }
               }
               wasPositive = true;
               if(b > highest) {
                  highest = b;
                  hThresh = (int)(highest*.75);
               }
            }
            sum+=Math.abs(b);
         }
         pcross+=SAMPLES-(zci>>1);
         //double freq = (count)/(2*(1.0/WINDOWS-(double)pcross/SAMPLE_RATE));
         double freq = count*WINDOWS/(2.0-2*(double)pcross/SAMPLES);
         //System.out.println(freq+"\t"+pcross);
         update_note(freq , (double)sum);
         pcross=0;
         highest = (int)(highest-1);
         hThresh = (int)(highest*.75);
         lowest = (int)(lowest-1);
         lThresh = (int)(lowest*.75);
      }
      stop_notes();
   }
   Receiver rec = null;
   public Tuna() {
      try {
         var mdia = MidiSystem.getMidiDeviceInfo();
         System.out.println(Arrays.toString(mdia));
         for(var mdi : mdia) {
            var md = MidiSystem.getMidiDevice(mdi);
            System.out.println(md.getMaxReceivers()+" "+md.getMaxTransmitters());
         }
         var md = MidiSystem.getMidiDevice(mdia[LOOPBACK_INDEX]);
         md.open();
         rec = md.getReceiver();
         rec.send(new ShortMessage(ShortMessage.START),0);
         rec.send(new ShortMessage(ShortMessage.PROGRAM_CHANGE,49,0),0);
      } catch(Exception e) {
         Logger.getGlobal().warning(e.toString());
      }
   }
   private int[] samples = new int[WINDOWS];
   private int index = 0;
   public void log(int c) {
      samples[index++] = c;
      if(index == samples.length){
         index=0;
         int sum =0;
         for(int i: samples) {
            sum+=i;
         }
         System.out.println(sum);
      }
   }
   int pNote = 0;
   long ts;
   double loudestNote=0;
   private static final double C1 = 8.1757989156;
   public void update_note(double freq, double mag) {
      loudestNote=loudestNote*.95;
      if(mag>loudestNote)
         loudestNote=mag;
      update_note(freq, (byte)(127*(mag/loudestNote)));
   }
   public void update_note(double freq, byte mag) {
      try {
         ts = System.currentTimeMillis();
         if(mag<10 || freq < C1) {
            rec.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, pNote, 64), ts);
            return;
         }
         //int note = (int)(Math.log(freq/C1)/Math.log(Math.pow(2,1.0/12.0))+.5);
         //double nError = freq/(C1*Math.pow(note,Math.pow(2,1.0/12.0)));
         double sfa4 = 12*Math.log(freq/440)/Math.log(2);
         int note = (int)(69+sfa4+.5);
         double nError = sfa4-(int)(sfa4+.5);
         if(note!=pNote && note > 0 && note < 127) {
            rec.send(new ShortMessage(ShortMessage.NOTE_OFF, 0, pNote, 64), ts);
            pNote=note;
            rec.send(new ShortMessage(ShortMessage.NOTE_ON, 0, note, mag), ts);
         }
         Logger.getGlobal().log(Level.FINE, String.format("%f\t%d\t%f%n",freq,mag,nError));
         byte bend = (byte)(nError*64+64);
         if(bend >= 0 && bend <= 127)
            rec.send(new ShortMessage(ShortMessage.PITCH_BEND, 0, 0,(byte)(nError*64+64)), ts);
      } catch (Exception e) {
         Logger.getGlobal().log(Level.WARNING,"Couldn't update note",e);
      }
   }
   public void stop_notes() {
      try {
         rec.send(new ShortMessage(ShortMessage.STOP,0,0,0), System.currentTimeMillis());
      } catch (Exception e) {}
   }
   public static interface SampleProvider {
      public boolean read(byte[] b);
   }
}
