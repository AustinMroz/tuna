import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.lang.Thread;

public class Test {
   public static void main(String[] args) {
      Logger.getGlobal().setLevel(Level.WARNING);
      testFile("tests/chirp2.wav");
   }
   private static void testFile(String s) {
      Tuna tuna = new Tuna();
      long lastTime = System.currentTimeMillis();
      try (var ais = AudioSystem.getAudioInputStream(new File(s))) {
         System.out.println(ais.getFormat());
         tuna.process(pcm -> {try {
            ensureDelay();
            return ais.read(pcm,0,pcm.length) != -1;
         } catch(IOException e) {
            return false;
         }});
      } catch (IOException e) {
         System.out.println("Could not open "+s+" for reading");
      } catch (UnsupportedAudioFileException e) {
         System.out.println("The file type of "+s+"is unsupported");
      }
   }
   static long lastTime = System.currentTimeMillis();
   private static void ensureDelay() {
      long currentTime = System.currentTimeMillis();
      long dt = currentTime-lastTime;
      if(dt< 1000/Tuna.WINDOWS) {
         try {
            Thread.sleep(1000/Tuna.WINDOWS-dt);
         } catch (java.lang.InterruptedException e) {}
      }
      lastTime = currentTime;
   }
}
